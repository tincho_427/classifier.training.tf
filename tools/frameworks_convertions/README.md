# Frameworks transformations and optimizations

## Keras -> Tensorflow

* **Step 1**: integrate to keras (.hdf5) model the network architecture, and save it in a new keras model (.h5)
* **Step 2**: convert keras model (.h5) to tensorflow model (NOT .pb)
* **Step 3**: convert tensorflow model to frozen protograph tensorflow model (.pb)
* **Step 4**: optimize tensorflow model with TensorRT (create a new .pb optimizated). Be carefull when use this because the optimizated model is going to work only in GPUs with the same architecture type (compute capability) of that the GPU where the model was optimized.

> Step 3 and step 4 are together in the same notebook because step 4 use some variables declarated in step 3.

## Part of the original documentation of this notebooks (the notebooks here were been modificated)

# About This Repo
This repository is for my YouTube video series [here](https://www.youtube.com/watch?v=AIGOSz2tFP8&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f), about optimizing deep learning model using TensorRT. We demonstrate optimizing LeNet-like model and YOLOv3 model, and get 3.7x and 1.5x faster for the former and the latter, respectively, compared to the original models. For the details and how to run the code, see the video below.

# List of Videos
1. [Optimizing Tensorflow Model to TensorRT](https://www.youtube.com/watch?v=AIGOSz2tFP8&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f)
2. [Visualizing Before and After TensorRT Optimization](https://www.youtube.com/watch?v=Hum7awcBffY&index=2&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f)
3. [Optimizing Keras Model to TensorRT](https://www.youtube.com/watch?v=ky4mFPewl8Y&index=3&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f)
4. [Train Keras Model Using Your Own Dataset](https://www.youtube.com/watch?v=9IEcI5JZWg8&index=4&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f)
5. [Restore and Use Stored Keras Model to Perform Inference](https://www.youtube.com/watch?v=h6X2THHnQ4s&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f&index=5)
6. [Optimizing YOLOv3 using TensorRT](https://www.youtube.com/watch?v=stBYLsq15lY&index=6&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f)
7. [Another YOLOv3 Detection Result (Native Tensorflow vs TensorRT optimized)](https://www.youtube.com/watch?v=IVUl61p6efU&list=PLkRkKTC6HZMwdtzv3PYJanRtR6ilSCZ4f&index=7)

# Pre-requirement
1. TensorRT: follow the tutorial [here](https://steemit.com/deeplearning/@ardianumam/installing-tensorrt-in-ubuntu-desktop) for Ubuntu dekstop or [here](https://steemit.com/deeplearning/@ardianumam/installing-tensorrt-in-jetson-tx2) for Jetson devices, to install tensorRT
