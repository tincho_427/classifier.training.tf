import os
import re
from shutil import copyfile
import argparse
import math
import random


def iterate_dir(source, dest, ratio):
    source = source.replace('\\', '/')
    dest = dest.replace('\\', '/')
    train_dir = os.path.join(dest, 'train')
    test_dir = os.path.join(dest, 'validation')
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)


    for root, dirs, files in os.walk(source, topdown=False):

        for folder in dirs:
            # do it per label folder
            label_folder = os.path.join(root, folder)

            images = [
                os.path.join(label_folder, filename)
                for filename in os.listdir(label_folder)
                if re.search(r'([a-zA-Z0-9\s_\\.\-\(\):])+(?i)(.jpg|.jpeg|.png)$', filename)
            ]

            num_images = len(images)
            num_test_images = math.ceil(ratio * num_images)
            print(f'Folder "{folder}" - {num_images} images '
                  f'({num_images - num_test_images}/{num_test_images})')

            for i in range(num_test_images):
                idx = random.randint(0, len(images) - 1)
                image_path = images[idx]

                filename = os.path.basename(image_path)

                origin = image_path
                dest_folder = os.path.join(
                    test_dir,
                    os.path.basename(os.path.dirname(image_path))
                )
                dest = os.path.join(dest_folder, filename)
                os.makedirs(dest_folder, exist_ok=True)
                copyfile(origin, dest)
                images.remove(images[idx])

            for image_path in images:
                filename = os.path.basename(image_path)

                origin = image_path
                dest_folder = os.path.join(
                    train_dir,
                    os.path.basename(os.path.dirname(image_path))
                )
                dest = os.path.join(dest_folder, filename)
                os.makedirs(dest_folder, exist_ok=True)
                # print(origin, dest)
                copyfile(origin, dest)


def main():
    parser = argparse.ArgumentParser(
        description=("Partition dataset of images into training and valdation "
                     "sets. From a folder with subfolders (labels) with images"
                     " inside for that class, create a new dataset folder with"
                     "the images splitted for training and validation"),
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        '-i', '--imageDir',
        help='Path to the folder where the image dataset is stored. If not specified, the CWD will be used.',
        type=str,
        default=os.getcwd()
    )
    parser.add_argument(
        '-o', '--outputDir',
        help='Path to the output folder where the train and valid dirs should be created. '
             'Defaults to the same directory as IMAGEDIR.',
        type=str,
        default=None
    )
    parser.add_argument(
        '-r', '--ratio',
        help='The ratio of the number of valid images over the total number of images. The default is 0.1.',
        default=0.2,
        type=float)
    args = parser.parse_args()

    if args.outputDir is None:
        args.outputDir = args.imageDir

    # Now we are ready to start the iteration
    iterate_dir(args.imageDir, args.outputDir, args.ratio)


if __name__ == '__main__':
    main()
