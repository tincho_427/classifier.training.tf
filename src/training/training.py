import argparse
import json
import os
from typing import List

import matplotlib.pyplot as plt
import tensorflow as tf
import yaml
from tensorflow.keras import models

from models import (
    DataGenConfig,
    ModelConfig,
    TrainingExperimentConfig,
)


def get_rescaling_function(rescale: float = 1.0, offset: float = 1.0):
    """
    Get a callable function for been applied as a "preprocess_function"
    """
    return lambda img: (img * rescale) + offset


PREPROCESSING_FUNCTIONS = {
    'rescaling': get_rescaling_function,
}


OPTIMIZERS = {
    'adam': tf.keras.optimizers.Adam,
    'RMSprop': tf.keras.optimizers.RMSprop,
}


MODELS = {
    'mobilenet-v2': tf.keras.applications.MobileNetV2,
}


def parse_args():
    desc = ('Test TensorFlow 1 detector from frozen graph.')
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-c', '--config_file',
                        help=('Path to the `config.yml` for training'),
                        type=str)
    args = parser.parse_args()
    return args


def load_model(
    model_config: ModelConfig,
    num_classes: int,
    debug: bool = False,
):
    model_config_dict = model_config.dict()
    model_name = model_config_dict.pop('model')
    model_config_dict['input_shape'] += (3,)  # add channel

    if model_name not in MODELS:
        raise NotImplementedError(f'Backbone "{model_name}" not implemented!')

    base_model = MODELS[model_name](**model_config_dict)
    base_model.trainable = False  # not train base model data

    # Add classification layer
    output_base = base_model.output
    # output_base = tf.keras.layers.Dropout(s0.2)(output_base)  # Regularize
    output = tf.keras.layers.Dense(
        num_classes,
        activation='softmax',
        name='classification_layer'
    )(output_base)

    # Create the model with the new classification layer
    model = models.Model(inputs=base_model.input,
                         outputs=output,
                         name=model_name)

    if debug:
        model.summary()

    return model, base_model


def compile_model_and_train(
    model: tf.python.keras.engine.training.Model,
    optimizer: 'tf.keras.optimizers',
    metrics: List[str],
    train_generator: tf.data.Dataset,
    valid_generator: tf.data.Dataset,
    epochs: int,
    callbacks: List['tf.keras.callbacks'],
    initial_epoch: int = 0,
    debug: bool = False,
):
    # ----- compile model -----
    model.compile(
        optimizer=optimizer,
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=metrics
    )
    if debug:
        # have to print the summary after compile when
        # change layer.trainable
        model.summary()

    # ----- train -----
    STEP_SIZE_TRAIN = train_generator.n // train_generator.batch_size
    STEP_SIZE_VALID = valid_generator.n // valid_generator.batch_size
    history = model.fit_generator(generator=train_generator,
                                  steps_per_epoch=STEP_SIZE_TRAIN,
                                  validation_data=valid_generator,
                                  validation_steps=STEP_SIZE_VALID,
                                  epochs=epochs,
                                  callbacks=callbacks,
                                  initial_epoch=initial_epoch,
                                  )
    return history


if __name__ == '__main__':
    args = parse_args()

    # ----- read and parse experiment config -----
    with open(args.config_file, 'r') as yaml_file:
        try:
            training_config = TrainingExperimentConfig.parse_obj(
                yaml.safe_load(yaml_file)
            )
        except yaml.YAMLError as exc:
            raise exc('Could not open the config file.')

    # ----- autocomplete the config with required data -----
    if (training_config.training.fine_tune_epochs > 0
       and training_config.compilation.fine_tune_optimizer is None):
        optimizer_config = training_config.compilation.optimizer
        training_config.compilation.fine_tune_optimizer = optimizer_config

    # ----- make a copy of the training config -----
    os.makedirs(training_config.training.results_folder, exist_ok=True)
    complete_training_config_path = os.path.join(
        training_config.training.results_folder,
        'complete_training_config.yml'
    )
    with open(complete_training_config_path, 'w') as outfile:
        yaml.dump(training_config.dict(), outfile, default_flow_style=False)

    # ----- load callbacks -----
    # -- checkpoint callback --
    training_config.callbacks.model_checkpoint.folder = os.path.join(
        training_config.training.results_folder,
        training_config.callbacks.model_checkpoint.folder
    )  # replace output folder with experiment results folder
    checkpoint_config = training_config.callbacks.model_checkpoint
    checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=os.path.join(
            checkpoint_config.folder,
            checkpoint_config.filename
        ),
        **checkpoint_config.dict()
    )
    os.makedirs(checkpoint_config.folder, exist_ok=True)

    # -- csv logger callback --
    training_config.callbacks.csv_logger.folder = os.path.join(
        training_config.training.results_folder,
        training_config.callbacks.csv_logger.folder,
    )  # replace output folder with experiment results folder
    csv_logger_config = training_config.callbacks.csv_logger
    csv_logger_callback = tf.keras.callbacks.CSVLogger(
        filename=os.path.join(
            csv_logger_config.folder,
            csv_logger_config.filename_csv
        ),
        append=csv_logger_config.append
    )
    os.makedirs(csv_logger_config.folder, exist_ok=True)

    # -- tensorboard callback --
    training_config.callbacks.tensorboard.log_dir = os.path.join(
        training_config.training.results_folder,
        training_config.callbacks.tensorboard.log_dir
    )  # replace output folder with experiment results folder
    tensorboard_config = training_config.callbacks.tensorboard
    tensorboard_callback = tf.keras.callbacks.TensorBoard(
        **tensorboard_config.dict()
    )
    os.makedirs(tensorboard_config.log_dir, exist_ok=True)

    # -- reduce learning rate on plateaw callback --
    reduce_lr_on_plateau_config = training_config.callbacks.reduce_lr_on_plateau
    reduce_lr_on_plateau_callback = tf.keras.callbacks.ReduceLROnPlateau(
        **reduce_lr_on_plateau_config.dict()
    )

    callbacks = [
        checkpoint_callback,
        csv_logger_callback,
        tensorboard_callback,
        reduce_lr_on_plateau_callback,
    ]

    # ----- load data generator ------
    data_aumentation_config = training_config.data_augmentation.dict()
    if training_config.data_augmentation.preprocessing_function:
        ppf_name = training_config.data_augmentation.preprocessing_function.name
        ppf_params = training_config.data_augmentation.preprocessing_function.params
        data_aumentation_config['preprocessing_function'] = PREPROCESSING_FUNCTIONS[ppf_name](**ppf_params.dict())
    # use the same data generator for training/validation images.
    datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        **data_aumentation_config
    )

    # ----- load dataset images -----
    print('> Loading images for training...')
    train_datagen_config = DataGenConfig(
        **training_config.train_data_gen.dict()
    )
    train_datagen_config.target_size = training_config.model.input_shape
    train_generator = datagen.flow_from_directory(
        directory=training_config.training.train_images_path,
        **train_datagen_config.dict()
    )
    # save labels
    with open(
        os.path.join(training_config.training.results_folder, "labels.json"),
        "w"
    ) as jsonfile:
        json.dump(train_generator.class_indices, jsonfile)

    print('> Loading images for validation...')
    validation_datagen_config = DataGenConfig(
        **training_config.validation_data_gen.dict()
    )
    validation_datagen_config.target_size = training_config.model.input_shape
    valid_generator = datagen.flow_from_directory(
        directory=training_config.training.validation_images_path,
        **validation_datagen_config.dict()
    )

    # ----- load model -----
    print('> Loading model...')
    compilation_config = training_config.compilation
    model, base_model = load_model(
        model_config=training_config.model,
        num_classes=train_generator.num_classes,
    )
    # save base model
    model.save(os.path.join(training_config.training.results_folder,
                            'pretrained_model.h5'))

    # ----- training -----
    print('> Compiling and starting training...')
    optimizer_params = compilation_config.optimizer.dict()
    optimizer_name = compilation_config.optimizer.name
    optimizer = OPTIMIZERS[optimizer_name](**optimizer_params)
    history = compile_model_and_train(
        model=model,
        optimizer=optimizer,
        metrics=compilation_config.metrics,
        train_generator=train_generator,
        valid_generator=valid_generator,
        epochs=training_config.training.epochs,
        callbacks=callbacks,
        debug=True,
    )
    # ----- training - fine tunning -----
    if training_config.training.fine_tune_epochs > 0:
        print('> Compiling and starting training (fine tunning)...')
        model.trainable = True  # set the entire model as trainable
        fine_tune_from_layer = training_config.training.fine_tune_from_layer
        if fine_tune_from_layer is not None:
            print(f'> Doing traibable from layer: {fine_tune_from_layer}')
            for layer in base_model.layers[:fine_tune_from_layer]:
                layer.trainable = False  # set some layers as non-trainable

        total_epochs = (
            training_config.training.epochs
            + training_config.training.fine_tune_epochs
        )

        ft_optimizer_params = compilation_config.fine_tune_optimizer.dict()
        ft_optimizer_name = compilation_config.fine_tune_optimizer.name
        ft_optimizer = OPTIMIZERS[ft_optimizer_name](**ft_optimizer_params)
        initial_epoch = 0 if len(history.epoch) == 0 else history.epoch[-1] + 1
        history_fine_tune = compile_model_and_train(
            model=model,
            optimizer=ft_optimizer,
            metrics=compilation_config.metrics,
            train_generator=train_generator,
            valid_generator=valid_generator,
            epochs=total_epochs,
            callbacks=callbacks,
            initial_epoch=initial_epoch,
            debug=True,
        )

    # # ----- evaluate the model -----
    # **NOTE**: This is not required because you could see the validation when training
    # print('> Starting evaluation...')
    # test_loss, test_acc, test_top_k_acc = model.evaluate_generator(
    #     generator=valid_generator,
    #     steps=STEP_SIZE_VALID
    # )
    # print(f'loss: {test_loss:.4f} - accuracy: {test_acc:.4f} - top_k accuracy: {test_top_k_acc:.4f}')

    # ----- plot the accuracy and loss -----
    print('> Plotting results...')
    acc, val_acc, loss, val_loss = [], [], [], []
    if training_config.training.epochs > 0:
        acc += history.history['acc']
        val_acc += history.history['val_acc']
        loss += history.history['loss']
        val_loss += history.history['val_loss']
        epochs_range = range(training_config.training.epochs)

    if training_config.training.fine_tune_epochs > 0:
        acc += history_fine_tune.history['acc']
        val_acc += history_fine_tune.history['val_acc']

        loss += history_fine_tune.history['loss']
        val_loss += history_fine_tune.history['val_loss']

        epochs_range = range(
            training_config.training.epochs
            + training_config.training.fine_tune_epochs
        )

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, val_acc, label='Validation Accuracy')
    if training_config.training.fine_tune_epochs > 0:
        plt.plot([training_config.training.epochs - 1,
                  training_config.training.epochs - 1],
                 plt.ylim(), label='Start Fine Tuning')
    plt.legend(loc='lower right')
    plt.grid()
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    if training_config.training.fine_tune_epochs > 0:
        plt.plot([training_config.training.epochs - 1,
                  training_config.training.epochs - 1],
                 plt.ylim(), label='Start Fine Tuning')
    plt.legend(loc='upper right')
    plt.grid()
    plt.title('Training and Validation Loss')
    plt.savefig(os.path.join(training_config.training.results_folder,
                             'training_acc_loss.png'))

    print(f'All results saved on: {training_config.training.results_folder}')
