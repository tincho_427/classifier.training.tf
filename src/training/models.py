from typing import List, Tuple, Optional, Union
from datetime import datetime

from pydantic import BaseModel, Extra
from typing_extensions import Literal


class TrainingProcessConfig(BaseModel, extra=Extra.forbid):
    train_images_path: str
    validation_images_path: str
    epochs: int = 10
    fine_tune_epochs: int = 0
    fine_tune_from_layer: int = 0  # fine-tunning (train all layers from layer x)
    results_folder: str = f'workspace/training_results/models/{str(datetime.now())}'


class ModelConfig(BaseModel, extra=Extra.forbid):
    model: str = 'mobilenet-v2'  # backbone
    weights: Optional[str] = 'imagenet'  # pre-trained weigths
    include_top: bool = False  # remove classification layers
    input_shape: Tuple = (224, 224)  # wished input_shape
    pooling: str = 'avg'


class PPFRescalingParams(BaseModel, extra=Extra.forbid):  # default to range [-1, 1]
    rescale: float = 1.0 / 255
    offset: float = -1


class PPFRescalingConfig(BaseModel, extra=Extra.forbid):
    name: str = 'rescaling'
    params: PPFRescalingParams


PREPROCESS_FUNCTIONS = Union[PPFRescalingConfig]


class ImageDataGeneratorParams(BaseModel, extra=Extra.forbid):
    featurewise_center: bool = False
    samplewise_center: bool = False
    featurewise_std_normalization: bool = False
    samplewise_std_normalization: bool = False
    zca_whitening: bool = False
    zca_epsilon: float = 1e-06
    rotation_range: int = 0
    width_shift_range: float = 0.0
    height_shift_range: float = 0.0
    brightness_range: Optional[List[float]] = None  # List[float, float]
    shear_range: float = 0.0
    zoom_range: float = 0.0
    channel_shift_range: float = 0.0
    fill_mode: str = "nearest"  # ["constant", "nearest", "reflect", "wrap"]
    cval: float = 0.0
    horizontal_flip: bool = False
    vertical_flip: bool = False
    rescale: Optional[float] = None  # 1.0/255
    preprocessing_function: Optional[PREPROCESS_FUNCTIONS] = None
    data_format: str = "channels_last"  # ["channels_first", "channels_last", "channels_last"]
    validation_split: float = 0.0
    dtype: Optional[str] = None  # default: 'float32'


class DataGenConfig(BaseModel, extra=Extra.forbid):
    target_size: Optional[Tuple] = None  # set on runtime
    color_mode: str = 'rgb'
    batch_size: int = 1
    class_mode: str = 'sparse'
    shuffle: bool = True
    seed: int = 427


class AdamOptimizerConfig(BaseModel, extra=Extra.forbid):
    name: Literal['adam'] = "adam"
    learning_rate: float = 0.001
    beta_1: float = 0.9
    beta_2: float = 0.999
    epsilon: float = 1e-07
    amsgrad: bool = False


class RMSpropOptimizerConfig(BaseModel, extra=Extra.forbid):
    name: Literal['RMSprop'] = 'RMSprop'
    learning_rate: float = 0.001
    rho: float = 0.9
    momentum: float = 0.0
    epsilon: float = 1e-07
    centered: bool = False


OPTIMIZERS = Union[AdamOptimizerConfig, RMSpropOptimizerConfig]


class CompilationConfig(BaseModel, extra=Extra.forbid):
    optimizer: OPTIMIZERS = AdamOptimizerConfig()
    fine_tune_optimizer: Optional[OPTIMIZERS] = None
    metrics: List[str] = ['accuracy', 'top_k_categorical_accuracy']  # top 5


class ModelCheckpointCallbackConfig(BaseModel, extra=Extra.forbid):
    filename: str = "weights.epoch_{epoch:03d}-val_acc_{val_acc:.2f}-val_loss_{val_loss:.2f}.hdf5"
    folder: str = "checkpoints"
    monitor: str = "val_loss"
    verbose: int = 1
    save_best_only: bool = False
    save_weights_only: bool = True
    mode: str = "auto"
    period: int = 1


class CSVLoggerCallbackConfig(BaseModel, extra=Extra.forbid):
    folder: str = ""
    filename_csv: str = "train.csv"
    append: bool = False


class TensorBoardCallbackConfig(BaseModel, extra=Extra.forbid):
    log_dir: str = "logs"


class ReduceLROnPlateauCallbackConfig(BaseModel, extra=Extra.forbid):
    """Reduce learning rate when a metric has stopped improving """
    monitor: str = "val_loss"
    factor: float = 0.1
    patience: int = 10
    verbose: int = 1
    mode: str = "auto"
    min_delta: float = 0.0001
    cooldown: int = 0
    min_lr: float = 0.0


class CallbacksConfig(BaseModel, extra=Extra.forbid):
    model_checkpoint: ModelCheckpointCallbackConfig = ModelCheckpointCallbackConfig()
    csv_logger: CSVLoggerCallbackConfig = CSVLoggerCallbackConfig()
    tensorboard: TensorBoardCallbackConfig = TensorBoardCallbackConfig()
    reduce_lr_on_plateau: ReduceLROnPlateauCallbackConfig = ReduceLROnPlateauCallbackConfig()


class TrainingExperimentConfig(BaseModel, extra=Extra.forbid):
    training: TrainingProcessConfig
    model: ModelConfig
    data_augmentation: ImageDataGeneratorParams = ImageDataGeneratorParams()
    train_data_gen: DataGenConfig
    validation_data_gen: DataGenConfig
    compilation: CompilationConfig = CompilationConfig()
    callbacks: CallbacksConfig = CallbacksConfig()
