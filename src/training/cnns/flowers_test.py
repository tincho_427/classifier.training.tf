from tensorflow.keras import layers, models


def get_backbone(debug: bool = False):

    num_classes = 5

    model = models.Sequential([
        # layers.experimental.preprocessing.Rescaling(1./255, input_shape=(180, 180, 3)),
        layers.Conv2D(16, 3, padding='same', activation='relu', input_shape=(180, 180, 3)),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(num_classes)
    ])

    if debug:
        model.summary()

    return model