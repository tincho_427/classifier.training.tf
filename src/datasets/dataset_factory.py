from .labelimg_dataset import LabelImgDataset  # , COCODataset


class DatasetFactory:
    AVAILABLE_DATASETS = {
        LabelImgDataset.NAME: LabelImgDataset,
        # COCODataset.NAME: COCODataset,
    }

    @staticmethod
    def build_dataset(
        dataset_key: str,
        images_path: str,
        annotations_path: str
    ):

        if dataset_key not in DatasetFactory.AVAILABLE_DATASETS:
            raise NotImplementedError(f'"{dataset_key}" dataset not exists!')

        dataset_ctr = DatasetFactory.AVAILABLE_DATASETS[dataset_key]
        return dataset_ctr.from_files(
            images_path=images_path,
            annotations_path=annotations_path,
        )
