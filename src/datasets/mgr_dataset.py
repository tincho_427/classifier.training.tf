import os
from typing import Optional

# import cv2

# from .models import DatasetAnnotation
# from .detection_dataset import DetectionDataset
# from detector.models import Annotation, BoundingBox

import tensorflow as tf

from models import DataGenConfig, ImageDataGeneratorParams
from datasets.classification_dataset import ClassificationDataset


class MGRKerasDataset(ClassificationDataset):
    """
    Loader dataset in the 'labelImg' tagger format.
    """
    NAME = 'mgr_keras'
    IMAGE_EXTENSIONS = ['.png', '.jpg', '.jpeg']


    @staticmethod
    def load_data(
        images_path: str,
        datagen_config: DataGenConfig,
        preprocessing_params,
    ):
        # new_dataset_instance = clss()

        datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            **preprocessing_params.dict()
        )

        generator = datagen.flow_from_directory(
            directory=images_path,
           **datagen_config.dict()
        )
        return generator





    #     annotations_path = os.path.abspath(annotations_path)
    #     images_path = os.path.abspath(images_path)
    #     annotations_to_add = clss.load_annotations_files(
    #         annotations_path=annotations_path,
    #         images_path=images_path
    #     )

    #     for annotation in annotations_to_add:
    #         # modify the path of the annotation
    #         annotation.image_path = os.path.join(images_path,
    #                                              annotation.image_path)
    #         new_dataset_instance.add_annotation(annotation)

    #     return new_dataset_instance