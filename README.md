# Object Detection Training using TensorFlow 1.x

This repository contains tools for training a classifier using TensorFlow 1.x and Keras Sequential API.
This time are going to classifier the [CIFAR10](https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz) dataset (10 classes and 6000 images per class).

## Installation

```sh
git submodule init
git submodule update
docker-compose -f docker/docker-compose.yml build
```

## Enter develop mode

```sh
docker-compose -f docker/docker-compose.yml up -d; docker exec -it classifiers_training_tf1 bash
```

For testing installation:
```sh
# tensorflow
python -c "import tensorflow as tf;print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
```

<!-- ## Prepare the training

> The example for traffic-lights is in the repository, so you could skip this and the next two steps.

For training a new model, is recommended to have the next folder structure with the data:
```sh
mkdir -p workspace/traffic_light_detector
cd workspace/traffic_light_detector

mkdir annotations        # To store annotated inputs
mkdir images             # To store training images
mkdir models             # To store training pipeline config and training output
mkdir pre-trained-models # To store pre-trained models
mkdir exported-models    # To store exported models after training
``` -->

<!-- ## Annotate images -->

<!-- ### Create the annotations in .xml format

For example, from `KITTI-360` took 100 images per wished class for training (100 green, 100 yellow, 100 red). Put that images on the workspace folder.

```sh
# on a virtualenv (outside docker) because serve a UI.
mkvirtualenv training
pip3 install labelImg==1.8.5
labelImg workspace/traffic_light_detector/images/

# if you had only one folder with all images tagged and have to partinionate the data in train/data to train:test=0.9:0.1:
python3 scripts/partition_dataset.py -x -i workspace/traffic_light_detector/images -r 0.1
# then you could delete the not splitted images (if you want)
rm workspace/traffic_light_detector/images/*.xml
rm workspace/traffic_light_detector/images/*.png
rm workspace/traffic_light_detector/images/*.jpg
``` -->

<!-- ### Convert the annotations in .xml format to .record (`TFRecord` format)

First of all, you have to create the `annotations/label_map.pbtxt` with the labels used on the tagged process:

```txt
item {
    id: 1
    name: 'green'
}

item {
    id: 2
    name: 'yellow'
}

item {
    id: 3
    name: 'red'
}
```

**NOTE**: **maybe** have to fix:
```sh
vim /usr/local/lib/python3.6/dist-packages/object_detection/utils/label_map_util.py  # line 132
# Replace by:
# with tf.io.gfile.GFile(path, 'r') as fid:
```

```sh
# inside docker
python3 scripts/generate_tfrecord.py -x workspace/traffic_light_detector/images/train -l workspace/traffic_light_detector/annotations/label_map.pbtxt -o workspace/traffic_light_detector/annotations/train.record
python3 scripts/generate_tfrecord.py -x workspace/traffic_light_detector/images/test -l workspace/traffic_light_detector/annotations/label_map.pbtxt -o workspace/traffic_light_detector/annotations/test.record
```

## Prepare the model for training

Prepare the `pipeline.config` file with the configuration for training and get the pre-trained model.

Download a pre-trained model from [tf_detection_zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf1_detection_zoo.md):

```sh
# example with ssd_mobilenet_v2_coco (for traffic-lights)

# download pre-trained model
cd workspace/traffic_light_detector/pre-trained-models
wget http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v2_coco_2018_03_29.tar.gz -O ssd_mobilenet_v2_coco_2018_03_29.tar.gz
tar -xzvf ssd_mobilenet_v2_coco_2018_03_29.tar.gz
rm ssd_mobilenet_v2_coco_2018_03_29.tar.gz
cd ..

# configure training pipeline
mkdir -p models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29
cp pre-trained-models/ssd_mobilenet_v2_coco_2018_03_29/pipeline.config models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/
# edit with the correct data:
vim models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/pipeline.config
# For example
#
# ssd.num_classes: 3
# train_config.batch_size: 8 # Increase/Decrease this value depending on the available memory (Higher values require more memory and vice-versa)
# fine_tune_checkpoint: "pre-trained-models/ssd_mobilenet_v2_coco_2018_03_29/
# fine_tune_checkpoint_type: "detection"
# train_input_reader.label_map_path: "annotations/label_map.pbtxt"
# train_input_reader.input_path: "annotations/train.record"
# eval_config.num_examples: 50  # bigger than the number of test images
# eval_input_reader.label_map_path: "annotations/label_map.pbtxt"
# eval_input_reader.input_path: "annotations/test.record"

# maybe remove (see note bellow): feature_extractor.batch_norm_trainable: true
```

> **NOTE**: For ssd mobilenet v2, you could remove from `pipeline.config`" the `batch_norm_trainable: true` as this field is deprecated now. If you don't remove this line, you are gona have an error when try to train the model. -->

<!-- ## Train the model

> Remember that if you add more images to make the **records** of the images!
> If you are using GPU and have more than one GPU available: `export CUDA_VISIBLE_DEVICES=0`

```sh
export CUDA_VISIBLE_DEVICES=0  # if are using gpu

cd workspace/traffic_light_detector
python3 ../../packages/tensorflow_models/research/object_detection/model_main.py \
    --model_dir=models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29 \
    --pipeline_config_path=models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/pipeline.config \
    --alsologtostderr
```

For monitoring the training of the model:
```sh
# in other terminal in the virtual env
pip3 install tensorboard
cd workspace/traffic_light_detector
tensorboard --logdir=models --port 6006
``` -->

<!-- ## Get model metrics (when is training)

> This step maybe is done at the same time while you are training (tested on CPU)

```sh
docker exec -it detectors_training_tf1 bash
export CUDA_VISIBLE_DEVICES=1  # if are using gpu

cd workspace/traffic_light_detector
python3 ../../packages/tensorflow_models/research/object_detection/model_main.py \
    --model_dir=models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29 \
    --pipeline_config_path=models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/pipeline.config \
    --checkpoint_dir=models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29 \
    --alsologtostderr
``` -->

<!-- ## Exporting inference graph

```sh
cd /dl/detectors/training_tf1_vol/workspace/traffic_light_detector
# edit the "trained_checkpoint_prefix" with the wished model for froze.
python3 /dl/detectors/training_tf1_vol/packages/tensorflow_models/research/object_detection/export_inference_graph.py \
    --input_type=image_tensor \
    --pipeline_config_path=models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/pipeline.config \
    --output_directory=exported-models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29 \
    --trained_checkpoint_prefix=models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/model.ckpt-33802
# recommended to not forget the labels
cp annotations/label_map.pbtxt exported-models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/saved_model/
```

Test on an image:
```sh
cd /dl/detectors/training_tf1_vol
python3 scripts/detect_on_image.py \
    --model=workspace/traffic_light_detector/exported-models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/frozen_inference_graph.pb \
    --labels=workspace/traffic_light_detector/exported-models/traffic_lights_ssd_mobilenet_v2_coco_2018_03_29/saved_model/label_map.pbtxt \
    --image=workspace/traffic_light_detector/images/test/green_305.png \
    --threshold=0.5
``` -->

<!-- ## Get metrics for the final model

In the "tools" folder could find a tool called `metrics_evaluator`. With this tool you could calculate the AP for each class, and the mAP for different iou thresholds.
You must create an file with the configuration for the metrics whished (recommmended to save this `yml` file in `tools/metrics_evaluator/configs`) and then run:
```sh
python3 tools/metrics_evaluator/get_detector_metrics.py tools/metrics_evaluator/configs/<example.yml>
```
 -->





### NOTES

# Prepare dataset

For create 0.8 train and 0.2 validation of data.
```sh
python3 tools/partition_dataset.py -i workspace/flowers/images/flower_photos -o workspace/flowers/images -r 0.2
```

## Prepare for training

### Data Augmentation

#### ImageDataGeneratorParams

More info:

* [keras_preprocessing](https://keras.io/api/preprocessing/image/)
* [Examples](https://machinelearningmastery.com/how-to-configure-image-data-augmentation-when-training-deep-learning-neural-networks/)


Arguments description:

```sh
featurewise_center: Boolean. Set input mean to 0 over the dataset, feature-wise.
samplewise_center: Boolean. Set each sample mean to 0.
featurewise_std_normalization: Boolean. Divide inputs by std of the dataset, feature-wise.
samplewise_std_normalization: Boolean. Divide each input by its std.
zca_epsilon: epsilon for ZCA whitening. Default is 1e-6.
zca_whitening: Boolean. Apply ZCA whitening.
rotation_range: Int. Degree range for random rotations.
width_shift_range: Float, 1-D array-like or int - float: fraction of total width, if < 1, or pixels if >= 1. - 1-D array-like: random elements from the array. - int: integer number of pixels from interval (-width_shift_range, +width_shift_range) - With width_shift_range=2 possible values are integers [-1, 0, +1], same as with width_shift_range=[-1, 0, +1], while with width_shift_range=1.0 possible values are floats in the interval [-1.0, +1.0).
height_shift_range: Float, 1-D array-like or int - float: fraction of total height, if < 1, or pixels if >= 1. - 1-D array-like: random elements from the array. - int: integer number of pixels from interval (-height_shift_range, +height_shift_range) - With height_shift_range=2 possible values are integers [-1, 0, +1], same as with height_shift_range=[-1, 0, +1], while with height_shift_range=1.0 possible values are floats in the interval [-1.0, +1.0).
brightness_range: Tuple or list of two floats. Range for picking a brightness shift value from.
shear_range: Float. Shear Intensity (Shear angle in counter-clockwise direction in degrees)
zoom_range: Float or [lower, upper]. Range for random zoom. If a float, [lower, upper] = [1-zoom_range, 1+zoom_range].
channel_shift_range: Float. Range for random channel shifts.
fill_mode: One of {"constant", "nearest", "reflect" or "wrap"}. Default is 'nearest'. Points outside the boundaries of the input are filled according to the given mode: - 'constant': kkkkkkkk|abcd|kkkkkkkk (cval=k) - 'nearest': aaaaaaaa|abcd|dddddddd - 'reflect': abcddcba|abcd|dcbaabcd - 'wrap': abcdabcd|abcd|abcdabcd
cval: Float or Int. Value used for points outside the boundaries when fill_mode = "constant".
horizontal_flip: Boolean. Randomly flip inputs horizontally.
vertical_flip: Boolean. Randomly flip inputs vertically.
rescale: rescaling factor. Defaults to None. If None or 0, no rescaling is applied, otherwise we multiply the data by the value provided (after applying all other transformations).
preprocessing_function: function that will be applied on each input. The function will run after the image is resized and augmented. The function should take one argument: one image (Numpy tensor with rank 3), and should output a Numpy tensor with the same shape.
data_format: Image data format, either "channels_first" or "channels_last". "channels_last" mode means that the images should have shape (samples, height, width, channels), "channels_first" mode means that the images should have shape (samples, channels, height, width). It defaults to the image_data_format value found in your Keras config file at ~/.keras/keras.json. If you never set it, then it will be "channels_last".
validation_split: Float. Fraction of images reserved for validation (strictly between 0 and 1).
dtype: Dtype to use for the generated arrays.
```

An example of results with "cat_dogs" dataset:

```sh
loss: 0.3506 - accuracy: 0.9640 - top_k accuracy: 1.0000
```

### Models considerations

Different models expects different inputs:

* `tf.keras.applications.MobileNetV2` model expects pixel values in [-1, 1]. To rescale the pixeles, use the preprocessing method included with the model.


### Training

Generate a `training_config.yml` file and then:
```sh
python3 src/training/training.py -c workspace/cat_dogs/models/training_config.yml
```

```sh
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root --port 6001
```
